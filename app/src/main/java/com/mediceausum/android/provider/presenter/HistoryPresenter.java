package com.mediceausum.android.provider.presenter;

import android.os.Bundle;

import com.mediceausum.android.provider.model.CustomException;
import com.mediceausum.android.provider.model.HistoryModel;
import com.mediceausum.android.provider.model.dto.common.HistoryItem;
import com.mediceausum.android.provider.model.dto.response.HistoryResponse;
import com.mediceausum.android.provider.model.listener.IModelListener;
import com.mediceausum.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.mediceausum.android.provider.view.adapter.HistoryAdapter;
import com.mediceausum.android.provider.view.iview.IHistoryView;

import org.jetbrains.annotations.NotNull;

public class HistoryPresenter extends BasePresenter<IHistoryView> implements IHistoryPresenter {


    public HistoryPresenter(IHistoryView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }

    @Override
    public void onResume() {
        super.onResume();
        getHistory();
    }

    @Override
    public void getHistory() {
        new HistoryModel(new IModelListener<HistoryResponse>() {

            public void onClickItem(int pos, HistoryItem data) {
                iView.moveToChat(data);
            }

            @Override
            public void onSuccessfulApi(@NotNull HistoryResponse response) {
                iView.setAdapter(new HistoryAdapter(response.getHistory(), this::onClickItem));
                iView.dismissProgressbar();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getHistoryDetails();
    }
}
