package com.mediceausum.android.provider.presenter.ipresenter;

import com.mediceausum.android.provider.model.dto.request.LoginRequest;

public interface ILoginPresenter extends IPresenter {
    void postLogin(LoginRequest request);
    void goToRegistration();
    void goToForgotPassword();
}