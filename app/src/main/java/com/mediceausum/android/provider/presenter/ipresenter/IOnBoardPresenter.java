package com.mediceausum.android.provider.presenter.ipresenter;

public interface IOnBoardPresenter extends IPresenter {

    void goToLogin();
}
