package com.mediceausum.android.provider.model;

import com.mediceausum.android.provider.model.dto.response.NotificationResponse;
import com.mediceausum.android.provider.model.listener.IModelListener;
import com.mediceausum.android.provider.model.webservice.ApiClient;
import com.mediceausum.android.provider.model.webservice.ApiInterface;

public class NotificationModel extends BaseModel<NotificationResponse> {

    public NotificationModel(IModelListener<NotificationResponse> listener) {
        super(listener);
    }

    public void getMissedDetails() {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getMissed());
    }

    @Override
    public void onSuccessfulApi(NotificationResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
