package com.mediceausum.android.provider.view.adapter.listener;

import com.mediceausum.android.provider.model.dto.response.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<Provider> {
}
