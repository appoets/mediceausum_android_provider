package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.model.dto.common.HistoryItem;
import com.mediceausum.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.mediceausum.android.provider.view.adapter.HistoryAdapter;

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryAdapter adapter);
    void initSetUp();
    void moveToChat(HistoryItem data);
}
