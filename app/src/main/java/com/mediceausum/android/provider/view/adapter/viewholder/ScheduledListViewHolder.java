package com.mediceausum.android.provider.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.mediceausum.android.provider.R;
import com.mediceausum.android.provider.common.utils.CodeSnippet;
import com.mediceausum.android.provider.model.dto.response.ScheduledListResponse;
import com.mediceausum.android.provider.view.adapter.listener.IScheduledListListener;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ScheduledListViewHolder extends BaseViewHolder<ScheduledListResponse, IScheduledListListener> {

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_doctor_name)
    TextView tvDoctorName;
    @BindView(R.id.tv_doctor_spl)
    TextView tvDoctorSpl;
    @BindView(R.id.tv_date_time)
    TextView tvDateTime;

    public ScheduledListViewHolder(View itemView, IScheduledListListener listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(ScheduledListResponse data) {
        String url = CodeSnippet.getImageURL(data.getUser().getPicture());
        showImage(ivProfile, url);
        tvDoctorName.setText(data.getUser().getFirstName() + data.getUser().getLastName());
        tvDoctorSpl.setText(" - ");
        if (data.getScheduleAt() != null && !data.getScheduleAt().equalsIgnoreCase("null"))
            tvDateTime.setText(CodeSnippet.parseDateToyyyyMMdd(data.getScheduleAt()));
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }

}
