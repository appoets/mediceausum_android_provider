package com.mediceausum.android.provider.presenter;

import android.support.annotation.NonNull;

import com.mediceausum.android.provider.model.CustomException;
import com.mediceausum.android.provider.model.LoginModel;
import com.mediceausum.android.provider.model.ProfileModel;
import com.mediceausum.android.provider.model.dto.request.LoginRequest;
import com.mediceausum.android.provider.model.dto.response.LoginResponse;
import com.mediceausum.android.provider.model.dto.response.ProfileResponse;
import com.mediceausum.android.provider.model.listener.IModelListener;
import com.mediceausum.android.provider.presenter.ipresenter.ILoginPresenter;
import com.mediceausum.android.provider.view.iview.ILoginView;

import org.jetbrains.annotations.NotNull;

import static com.mediceausum.android.provider.MyApplication.getApplicationInstance;


public class LoginPresenter extends BasePresenter<ILoginView> implements ILoginPresenter {

    public LoginPresenter(ILoginView iView) {
        super(iView);
    }

    @Override
    public void goToForgotPassword() {
        iView.goToForgotPassword();
    }

    @Override
    public void goToRegistration() {
        iView.goToRegistration();
    }

    @Override
    public void postLogin(LoginRequest request) {
        iView.showProgressbar();
        new LoginModel(new IModelListener<LoginResponse>() {
            @Override
            public void onSuccessfulApi(@NonNull LoginResponse response) {
                getApplicationInstance().setAccessToken(response.getAccessToken());
                getUserInfo();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
                iView.dismissProgressbar();
            }
        }).postLogin(request);
    }

    private void getUserInfo() {
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                getApplicationInstance().setCurrency(response.getCurrency());
                iView.dismissProgressbar();
                iView.goToHome();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getUserDetails();
    }
}
