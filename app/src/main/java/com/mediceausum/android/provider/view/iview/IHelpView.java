package com.mediceausum.android.provider.view.iview;

import com.mediceausum.android.provider.model.dto.response.HelpResponse;
import com.mediceausum.android.provider.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
