package com.mediceausum.android.provider.presenter.ipresenter;

import com.mediceausum.android.provider.model.dto.request.AvailabilityRequest;
import com.mediceausum.android.provider.model.dto.request.LocationRequest;
import com.mediceausum.android.provider.model.dto.request.RatingRequest;

public interface IHomePresenter extends IPresenter {
    void goToHelp();

    void goToHistory();

    void goToSchedule();

    void goToAvailability();

    void updateDeviceToken();

    void getUserDetails();

    void changeAvailability(AvailabilityRequest request);

    void updateLocation(LocationRequest request);

    void getInvoiceDetails(String id);

    void postRating(String id, RatingRequest request);

    void moveToNotification();

    void getNotificationCount();
}