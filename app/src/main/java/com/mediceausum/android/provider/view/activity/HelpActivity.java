package com.mediceausum.android.provider.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.mediceausum.android.provider.R;
import com.mediceausum.android.provider.model.dto.response.HelpResponse;
import com.mediceausum.android.provider.presenter.HelpPresenter;
import com.mediceausum.android.provider.presenter.ipresenter.IHelpPresenter;
import com.mediceausum.android.provider.view.iview.IHelpView;

import butterknife.BindView;
import butterknife.OnClick;

import static com.mediceausum.android.provider.BuildConfig.BASE_URL;

public class HelpActivity extends BaseActivity<IHelpPresenter> implements IHelpView {

    @BindView(R.id.ibBack)
    ImageButton ibBack;

    @BindView(R.id.ivPhone)
    ImageView ivPhone;
    @BindView(R.id.ivEmail)
    ImageView ivEmail;
    @BindView(R.id.ivWeb)
    ImageView ivWeb;

    String ContactNumber="",ContactEmail="";

    @Override
    int attachLayout() {
        return R.layout.activity_help;
    }

    @Override
    IHelpPresenter initialize() {
        return new HelpPresenter(this);
    }

    @OnClick({R.id.ibBack,R.id.ivPhone,R.id.ivEmail,R.id.ivWeb})
    public void onViewClicked(View view){
        switch (view.getId()){

            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.ivPhone:
                dialPhone();
                break;

            case R.id.ivEmail:
                openEmail();
                break;

            case R.id.ivWeb:
                launchBrowser();
                break;
        }
    }

    private void launchBrowser() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BASE_URL));
        if (browserIntent.resolveActivityInfo(getPackageManager(), browserIntent.getFlags()) != null)
            startActivity(browserIntent);
        else
            showSnackBar(getString(R.string.unable_to_launch_browser));
    }

    private void openEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", ContactEmail, null));
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name)+" Help");
        startActivity(Intent.createChooser(emailIntent, "Send Email"));
    }

    private void dialPhone() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ContactNumber));
        if (intent.resolveActivityInfo(getPackageManager(), intent.getFlags()) != null)
            startActivity(intent);
        else
            showSnackBar(getString(R.string.call_feature_not_supported));
    }

    @Override
    public void updateHelpDetails(HelpResponse response) {
        ContactNumber = response.getContactNumber();
        ContactEmail = response.getContactEmail();
    }
}
