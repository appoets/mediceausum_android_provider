package com.mediceausum.android.provider.presenter.ipresenter;

public interface IHelpPresenter extends IPresenter {
    void getHelpDetails();
}