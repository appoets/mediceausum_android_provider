package com.mediceausum.android.provider.presenter.ipresenter;

import com.mediceausum.android.provider.model.dto.request.ResetPasswordRequest;

public interface IForgotChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void resetPassword(ResetPasswordRequest resetPasswordRequest);
}